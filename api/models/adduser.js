const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let productSchema = new mongoose.Schema({
    name: { type: String },
    email: { type: String },
    password: { type: String },
    role : { type: String},
    is_delete:{type: Boolean,default:false}
});
const adduser = mongoose.model('adduser', productSchema);
module.exports = adduser;