const productModel = require('../models/productlist');
const adduserlist = require('../models/adduser');
const signup = require('../models/signup');
const jwt = require('jsonwebtoken')
module.exports = {
    createuser: createuser,
    adminLogin:adminLogin,
    product:product,
    listing:listing,
    adduser:adduser,
    adduserlisting:  adduserlisting,
    deleteitem:deleteitem,
  deleteuserlist:deleteuserlist

}

function createuser(req, res) {
    let user = new signup({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        role:"user",
        is_active :req.body.is_active,
        is_delete: req.body.is_delete
    
    })
    user.save(user, function (err, response) {
        if (err) {
            res.json({
                code: 404,
                message: 'admin not Added'
            })
        } else {
            res.json({
                code: 200,
                data: response
            })
            console.log('Successfully created');

        }

    })
}
function adminLogin (req, res) {
    console.log(req.body)
    var email = req.body.email;
    var password = req.body.password;
    signup.findOne({
        email: req.body.email,
        password: req.body.password,

        // is_delete: false
    }, function( err ,admin){
        console.log("err",err);
        console.log(admin)
        if(err){
            console.log("error");
            res.json({
                code:404,
                message: "error",
                data:null,
            });
        }
        else if(admin){
            console.log(admin , "admin");
            const token =jwt.sign(admin.toJSON(), 'aihsas' , {
                expiresIn:1404
            });
            console.log("abc", token);


        res.json({
            code:200,
            message: "success",
            data:admin,
            token:token
        });
        
        }
        
    });
}
function product (req,res) {
    console.log('req.body-->', req.body)
    var obj={
   pname: req.body.pname,
   pcost: req.body.pcost,
   pdescription: req.body.pdescription,
   status: req.body.status
    };
    var record = new productModel(obj);
    record.save(function(err,data){
        console.log(data);
        if(err){
            res.json({code: 400,data:{}});
        }else{
            res.json({code: 200,data:data});
        }
      })
  }
  function listing(req,res) {
    productModel.find(function(err,data){
        res.json(data);
    })
};
function adduser (req,res) {
    console.log('req.body-->', req.body)
    var obj={
   name: req.body.name,
   email: req.body.email,
   password: req.body.password,
   role: req.body.role
    };
    var record = new adduserlist(obj);
    record.save(function(err,data){
        console.log(data);
        if(err){
            res.json({code: 400,data:{}});
        }else{
            res.json({code: 200,data:data});
        }
      })
  }
  function  adduserlisting(req,res) {
    adduserlist.find(function(err,data){
        res.json(data);
    })
};  

function deleteitem (req,res){
    var id = req.swagger.params.id.value;
    productModel.findByIdAndRemove({_id:id},function (err,data){
        if (err){throw err;}
        else{res.json(data)}
    })

}
 function deleteuserlist (req,res){
    var id = req.swagger.params.id.value;
    productModel.findByIdAndRemove({_id:id},function (err,data){
        if (err){throw err;}
        else{res.json(data)}
    })

}


